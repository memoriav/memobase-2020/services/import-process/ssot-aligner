package ch.memobase

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scala.io.Source

class DocumentUtilsSpec extends AnyFlatSpec with Matchers {
  "Extract original id" should "equal 18BD1034_B14" in {
    val f = Source.fromFile("src/test/resources/testrecord.ntriples")
    val data = f.mkString
    DocumentUtils.extractOriginalId(data)._2 shouldBe Some("18BD1034_B14")
  }
}
