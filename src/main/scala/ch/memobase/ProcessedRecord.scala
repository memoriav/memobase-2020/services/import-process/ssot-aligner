package ch.memobase

final case class ProcessedRecord(
    data: String,
    isImport: Boolean,
    report: Report
)

