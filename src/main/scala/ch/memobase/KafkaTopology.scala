/*
 * ssot-aligner
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Named
import org.apache.kafka.streams.scala.ImplicitConversions.*
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.kstream.{Branched, KStream}
import org.apache.kafka.streams.scala.serialization.Serdes
import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success}

class KafkaTopology extends AppSettings with Logging {

  import Serdes.*

  private val pgClient = PostgresClient(postgresHost, postgresUser, postgresPwd)

  def build: Topology = {
    val builder = new StreamsBuilder

    val source =
      builder.stream[String, String](inputTopic)

    val withMemobaseId = source
      .split(Named.as("memid-"))
      .branch( (k, _) => k != null && k.nonEmpty, Branched.as("true"))
      .defaultBranch(Branched.as("false"))

    withMemobaseId.get("memid-false").foreach(doc => report(doc.mapValues((k, v) =>
      ProcessedRecord(
        v,
        isImport = false,
        Report(k, reportingStepName, ProcessFatal, "No record ID found")
      )
    )))

    val withOriginalId =
      withMemobaseId.get("memid-true").map(_.mapValues(v => DocumentUtils.extractOriginalId(v)))

    val mode = withOriginalId.map(alignWithDB(_)
      .split(Named.as("mode-"))
      .branch( (_, v) => v.isImport, Branched.as("import"))
      .defaultBranch(Branched.as("deletion"))
    ).getOrElse(Map())

    mode.get("mode-import").foreach(sendRecordDownstream)

    mode.get("mode-import").foreach(report)
    mode.get("mode-deletion").foreach(report)

    builder.build()
  }

  private def alignWithDB(
      source: KStream[String, (String, Option[String])]
  ): KStream[String, ProcessedRecord] =
// TODO: Extend alignment to other use cases: Merging, deleting
    source.mapValues((k, v) => {
      logger.debug(s"Processing message $k")
      val shortId = k.substring("https://memobase.ch/record/".length)
      val report = pgClient.createOrUpdate(shortId, v._1, v._2) match {
        case Success(_) =>
          logger.debug(s"Processing $k successful")
          Report(
            k,
            reportingStepName,
            ProcessSuccess,
            "Record successfully created or updated"
          )
        case Failure(f) =>
          logger.warn(s"Error processing $k: ${f.getMessage}")
          Report(k, reportingStepName, ProcessFatal, f.getMessage)
      }
      ProcessedRecord(v._1, isImport = true, report)
    })

  private def sendRecordDownstream(
      records: KStream[String, ProcessedRecord]
  ): Unit =
    records
      .mapValues(_.data)
      .to(outputTopic)

  private def report(record: KStream[String, ProcessedRecord]): Unit =
    record.mapValues(_.report.toString).to(reportTopic)
}
