package ch.memobase

object DocumentUtils {
  def extractOriginalId(
      data: String
  ): (String, Option[String]) = {
    val lines = data.split("\n")
    val recordIdentifierBlankNodeIds = lines
      .filter(l =>
        l.startsWith("<https://memobase.ch/record/") && l.contains(
          "<https://www.ica.org/standards/RiC/ontology#identifiedBy>"
        )
      )
      .map(_.split(" ")(2))
    val recordIdentifierBlankNodes = lines.filter(l =>
      recordIdentifierBlankNodeIds.exists(id => l.startsWith(id))
    )
    val originalIdBlankNodeId = recordIdentifierBlankNodes.collectFirst {
      case l
          if l.contains(
            "<https://www.ica.org/standards/RiC/ontology#type>"
          ) && l.contains("original") =>
        l.split(" ")(0)
    }
    originalIdBlankNodeId match {
      case Some(id) =>
        (
          data,
          recordIdentifierBlankNodes
            .collectFirst {
              case l
                  if l.startsWith(id) && l.contains(
                    "<https://www.ica.org/standards/RiC/ontology#identifier>"
                  ) =>
                l.split(" ")(2)
                  .split("\"")(1)
            }
        )
      case None => (data, None)
    }
  }
}
