package ch.memobase

import org.apache.logging.log4j.scala.Logging

import java.sql.*
import scala.util.{Failure, Try}

class PostgresClient private (url: String, user: String, pwd: String) extends Logging {
  private var connection: Connection = getConnection

  private def getConnection: Connection = {
    logger.info(s"Establishing connection to PostgreSQL database on $url")
    val connection = DriverManager.getConnection(url, user, pwd)
    connection.setAutoCommit(true)
    connection
  }

  def createOrUpdate(
      id: String,
      record: String,
      originalId: Option[String],
      retry: Boolean = true
  ): Try[Int] = {
    logger.debug(s"Creating or updating record $id on PostgreSQL database")
    PostgresClient
      .createOrUpdateRecord(connection)(id)(record)(originalId)
      .recoverWith {
        case _: SQLException if retry =>
          logger.warn("Database access error: Reconnecting")
          reconnect()
          createOrUpdate(id, record, originalId, retry = false)
        case _: SQLTimeoutException if retry =>
          logger.warn("Database timeout error: Reconnecting")
          reconnect()
          createOrUpdate(id, record, originalId, retry = false)
        case e =>
          logger.error(s"An unrecoverable error happened: $e")
          Failure(e)
      }
  }

  private def delete(id: String, retry: Boolean = true): Try[Int] =
    logger.debug(s"Deleting record $id on PostgreSQL database")
    PostgresClient
      .deleteRecord(connection)(id)
      .recoverWith {
        case _: SQLException if retry =>
          logger.warn("Database access error: Reconnecting")
          reconnect()
          delete(id, retry = false)
        case _: SQLTimeoutException if retry =>
          logger.warn("Database timeout error: Reconnecting")
          reconnect()
          delete(id, retry = false)
        case e =>
          logger.error(s"An unrecoverable error happened: $e")
          Failure(e)
      }

  private def merge(
      id: String,
      mergeFun: String => Try[String],
      retry: Boolean = true
  ): Try[Int] = {
    logger.debug(s"Merge record $id on PostgreSQL database")
    PostgresClient
      .getRecordQuery(connection)(id)
      .flatMap(res => {
        res.next
        mergeFun(res.getString(1))
      })
      .flatMap(merged =>
        PostgresClient
          .partialUpdateRecord(connection)(id)(merged)
      )
  }
    .recoverWith {
      case _: SQLException if retry =>
        logger.warn("Database access error: Reconnecting")
        reconnect()
        merge(id, mergeFun, retry = false)
      case _: SQLTimeoutException if retry =>
        logger.warn("Database timeout error: Reconnecting")
        reconnect()
        merge(id, mergeFun, retry = false)
      case e =>
        logger.error(s"An unrecoverable error happened: $e")
        Failure(e)
    }

  private def reconnect(): Unit = {
    connection = getConnection
  }

}

object PostgresClient {
  def apply(
      url: String,
      user: String,
      pwd: String
  ): PostgresClient = {
    new PostgresClient(url, user, pwd)
  }

  private val tableName = "memobase_record"

  private val getById: Connection => String => String => Try[ResultSet] =
    conn =>
      stmt =>
        id => {
          val preparedStatement = conn.prepareStatement(stmt)
          preparedStatement.setString(1, id)
          Try(preparedStatement.executeQuery)
        }

  private def getIds(id: String): Try[(String, String)] = {
    val split = id.split('-')
    Try((split(0), s"${split(0)}-${split(1)}"))
  }

  private val getRecordStmtTmpl =
    s"""SELECT record FROM "$tableName" WHERE "memobase_id" = ?"""
  private val getRecordQuery: Connection => String => Try[ResultSet] = conn =>
    id => getById(conn)(getRecordStmtTmpl)(id)

  private val deleteStmtTmpl =
    s"""UPDATE "$tableName" SET status = 'DELETED' WHERE "memobase_id" = ?"""
  private val deleteRecord: Connection => String => Try[Int] = conn =>
    id => {
      val preparedStatement = conn.prepareStatement(deleteStmtTmpl)
      preparedStatement.setString(1, id)
      preparedStatement.setQueryTimeout(20)
      Try(preparedStatement.executeUpdate)
    }

  private val createOrUpdateStmtTmpl =
    s"""INSERT INTO "$tableName" (memobase_id, original_id, record_set_id, institution_id, record, status) VALUES (?, ?, ?, ?, ?, 'CREATED') ON CONFLICT ("memobase_id") DO UPDATE SET (original_id, record, status) = (?, ?, 'UPDATED')"""

  private val createOrUpdateRecord
      : Connection => String => String => Option[String] => Try[Int] =
    conn =>
      id =>
        record =>
          originalId => {
            getIds(id) flatMap (ids => {
              val preparedStatement =
                conn.prepareStatement(createOrUpdateStmtTmpl)
              preparedStatement.setString(1, id)
              preparedStatement.setString(2, originalId.getOrElse(""))
              preparedStatement.setString(3, ids._2)
              preparedStatement.setString(4, ids._1)
              preparedStatement.setString(5, record)
              preparedStatement.setString(6, originalId.getOrElse(""))
              preparedStatement.setString(7, record)
              preparedStatement.setQueryTimeout(20)
              Try(preparedStatement.executeUpdate)
            })
          }

  private val partialUpdateStmtTmpl =
    s"""UPDATE SET (record, status) = (?, 'MERGED') WHERE "memobase_id" = ?"""

  private val partialUpdateRecord: Connection => String => String => Try[Int] =
    conn =>
      id =>
        record => {
          val preparedStatement = conn.prepareStatement(partialUpdateStmtTmpl)
          preparedStatement.setString(1, record)
          preparedStatement.setString(2, id)
          preparedStatement.setQueryTimeout(20)
          Try(preparedStatement.executeUpdate)
        }
}
