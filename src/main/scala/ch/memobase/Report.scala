package ch.memobase

import java.util.Calendar
import java.text.SimpleDateFormat

import Report.*

sealed trait ProcessStatus {
  val value: String
}

case object ProcessSuccess extends ProcessStatus {
  override val value: String = "SUCCESS"
}

case object ProcessIgnore extends ProcessStatus {
  override val value: String = "IGNORE"
}

case object ProcessWarning extends ProcessStatus {
  override val value: String = "WARNING"
}

case object ProcessFatal extends ProcessStatus {
  override val value: String = "FATAL"
}

final case class Report(
                         documentId: String,
                         step: String,
                         processStatus: ProcessStatus = ProcessSuccess,
                         message: String = "",
                         timestamp: String = {
                           val now = Calendar.getInstance().getTime
                           val format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS")
                           format.format(now)
                         }
                       ) {
  override def toString: String = {
    s"""{"step":"$step",
        "stepVersion":"${getClass.getPackage.getImplementationVersion}",
        "timestamp":"$timestamp",
        "id":"$documentId",
        "status":"${processStatus.value}",
        "message":"${escapeApostrophes(message)}"}"""
  }
}

object Report {
  private def escapeApostrophes(value: String): String = {
    value.replaceAll(""""""", """\\"""")
  }
}
