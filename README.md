# Single Source of Truth Aligner

Performs the alignment of newly imported documents with a possible
previous copy (i.e. a document with the same document id) in the SSOT
database. There are four cases two be covered:

* __Create__: The document is not yet registered in the SSOT. The
  status of the respective document in the SSOT is set to `created`.
* __Update__: The document in the SSOT is entirely overwritten by the newly
  imported document. The status of the document is set to `updated`.
* __Partial update__: Specific parts of the document in the SSOT are
  amended or overwritten by the newly imported document. The status of the document is set to `updated`.
* __Delete__: The status of the document is set to `deleted`, the
  respective content left as is.

## Configuration

The configuration is provided via environmental variables:

* `APPLICATION_ID`: Identifier used by the internal Kafka Streams client
* `DEBUG_LEVEL`: Log level (defaults to `warn`)
* `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of the Kafka
  cluster's bootstrap servers 
* `KAFKA_STREAMS_SETTINGS`: Additional Kafka settings as key-value-pairs,
  separated by commas (optional)
* `POSTGRES_DATABASE`: Name of PostgreSQL database
* `POSTGRES_HOST`: Host name of PostgreSQL database
* `POSTGRES_PASSWORD`: Passwort of application's PostgreSQL user
* `POSTGRES_PORT`: Port of PostgreSQL database
* `POSTGRES_USER`: Application's PostgreSQL user
* `REPORTING_STEP_NAME`: Application's name used in reports
* `TOPIC_IN`: Kafka input topic
* `TOPIC_OUT`: Kafka output topic
* `TOPIC_PROCESS`: Kafka reporting topic
* `SECURITY_PROTOCOL: Protocol used to communicate with brokers. Valid values are: `PLAINTEXT`, `SSL`, `SASL_PLAINTEXT`, `SASL_SSL`
* `SSL_KEYSTORE_TYPE: The file format of the key store file. This is optional for client. 
* `SSL_KEYSTORE_LOCATION: The location of the key store file. This is optional for client and can be used for two-way authentication for client
* `SSL_KEYSTORE_KEY`: Private key in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with PKCS#8 keys. If the key is encrypted, key password must be specified using `SSL_KEY_PASSWORD`
* `SSL_KEYSTORE_CERTIFICATE_CHAIN`: Certificate chain in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with a list of X.509 certificates
* `SSL_TRUSTSTORE_TYPE: The file format of the trust store file. The values currently supported by the default `ssl.engine.factory.class` are `JKS`, `PKCS12` and `PEM`
* `SSL_TRUSTSTORE_LOCATION: The location of the trust store file
* `SSL_TRUSTSTORE_CERTIFICATES`: Trusted certificates in the format specified by `SSL_TRUSTSTORE_TYPE`. Default SSL engine factory supports only PEM format with X.509 certificates
* `SSL_KEY_PASSWORD`: The password of the private key in the Kafka key store file or the PEM key specified in `SSL_KEYSTORE_KEY`.

Furthermore, the following file must be present when communicating with a protected Kafka cluster:

* Key store file in the format defined in `SSL_KEYSTORE_TYPE` and at the path defined in `SSL_KEYSTORE_LOCATION`
* Key store certificate chain in PEM format at the path defined in `SSL_KEYSTORE_CERTIFICATE_CHAIN`
* Key store key in PEM format at the path defined in `SSL_KEYSTORE_KEY`
* Trust store file in the format defined in `SSL_TRUSTSTORE_TYPE` and at the path defined in `SSL_TRUSTSTORE_LOCATION`
* Trust store certificates in PEM format at the path defined in `SSL_TRUSTSTORE_CERTIFICATES`
