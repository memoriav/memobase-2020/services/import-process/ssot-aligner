import Dependencies.*

ThisBuild / scalaVersion := "3.5.2"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  } else {
    None
  }
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "SSOT Aligner",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case "log4j.properties"                      => MergeStrategy.first
      case "log4j2.xml"                            => MergeStrategy.first
      case "module-info.class"                     => MergeStrategy.discard
      case "META-INF/versions/9/module-info.class" => MergeStrategy.discard
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    assembly / mainClass := Some("ch.memobase.App"),
    git.useGitDescribe := true,
    libraryDependencies ++= Seq(
      http4sDsl,
      http4sServer,
      kafkaStreams.cross(CrossVersion.for3Use2_13),
      log4jApi,
      log4jCore,
      log4jScala,
      log4jSlf4j,
      postgresqlJDBC,
      kafkaStreamsTestUtils % Test,
      scalaTest % Test
    )
  )
