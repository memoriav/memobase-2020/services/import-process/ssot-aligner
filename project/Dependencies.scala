import sbt.*

object Dependencies {
  lazy val http4sV = "0.23.27"
  lazy val kafkaV = "3.8.0"
  lazy val log4jV = "2.23.1"
  lazy val scalaTestV = "3.2.18"

  lazy val http4sDsl = "org.http4s" %% "http4s-dsl" % http4sV
  lazy val http4sServer = "org.http4s" %% "http4s-ember-server" % http4sV
  lazy val kafkaStreams = "org.apache.kafka" %% "kafka-streams-scala" % kafkaV
  lazy val kafkaStreamsTestUtils =
    "org.apache.kafka" % "kafka-streams-test-utils" % kafkaV
  lazy val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jV
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jV
  lazy val log4jScala =
    "org.apache.logging.log4j" %% "log4j-api-scala" % "13.1.0"
  lazy val log4jSlf4j = "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4jV
  lazy val postgresqlJDBC = "org.postgresql" % "postgresql" % "42.7.3"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalaTestV
}
